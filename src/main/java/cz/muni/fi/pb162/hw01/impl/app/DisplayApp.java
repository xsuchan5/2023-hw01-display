package cz.muni.fi.pb162.hw01.impl.app;

import cz.muni.fi.pb162.hw01.cmd.Application;


/**
 * Display application
 */
public class DisplayApp implements Application<DisplayAppOptions> {

    /**
     * Runtime logic of the application
     *
     * @param options
     * @return exit status code
     */
    public int run(DisplayAppOptions options) {
        // TODO: replace body with your implementation
        throw new UnsupportedOperationException();
    }
}
